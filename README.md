1. Файловая система
    1. `cd`
    1. `ls`
    1. `pwd`
    1. `touch`
    1. `mkdir`
    1. `cp`
    1. `mv`
    1. `rm`
    1. `rmdir`
    1. `du`
    1. `df`
1. Работа с файлами
    1. `cat`
    1. `truncate`
    1. `nano`
    1. `wc`
    1. `head`
    1. `tail`
    1. `diff`
    1. `zip`, `unzip`
1. Поиск
    1. `find`
    1. `grep`
    1. `pwd`
1. Системные утилиты
    1. `man`
    1. `w`, `who`, `whoami`
	1. `date`
	1. `uname`
	1. `lsusb`
	1. `lsblk`
	1. `lscpu`
	1. `lspci`
	1. `blkid`
	1. `dmidecode`
1. Процессы
	1. `top`
	1. `ps`
	1. `kill`
	1. `bg`, `fg`
1. Сетевые функции
	1. `ping`
	1. `curl`
	1. `wget`
	1. `dig`
	1. `arp-scan`
	1. `ss`
	1. `ncat`
	1. `socat`
	1. `nmap`
	1. `ip`
		1. `link`
		1. `addr`
	1. `telnet`
	1. `ssh`
1. Переменные
	1. `echo`
1. Разрешения файлов и директорий
	1. `chmod`
	1. `chown`
	1. `chgrp`
	1. `su`
	1. `sudo`
1. Перенаправление вывода
	1. `xargs`
    1. `sort`
    1. `uniq`
1. Скрипты
	1. `awk`
    1. `sed`
    1. `cut`
    1. `seq`
    1. `rev`
    1. `time`
    1. `realpath`
1. Установка программ
	1. `apt`, `apt-get`
	1. `dpkg`
1. Система контроля версий `git`
1. Компиляция программ
	1. `make`
	1. `gcc`
	1. `pkg-config`
	1. `automake`
	1. `cmake`
1. Работа с графикой
	1. `convert`
	1. `pdftk`
